﻿**Voice training**

If you prefer to watch a youtube tutorial, see: https://www.youtube.com/watch?v=bmOTADDXVh8

If the bot has trouble understanding you then you can submit a voice training set to provide voice samples to the bot.

1. Download and run the latest version of the trainer from https://gitlab.com/overlord-bot/trainer/-/releases
2. Upload the voice Samples to the OverlordBot discord server #voice-training channel: https://discord.gg/9RqyTJt (Alternative upload options are available in the Discord channel)
3. Set your name on the OverlordBot server to include your callsign.
4. Wait for the training run to be performed by the developer. They are done weekly on Sundays and are usually trained and deployed by Monday (JST).
  * If you are on the Discord server when the training finishes then you will recieve a notification that training has been completed.