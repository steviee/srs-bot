﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Tests.Mocks;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Tests.AwacsResponder
{
    [TestClass]
    public class AwacsReceiverRecognitionTests
    {

        [TestMethod]
        public async Task GivenAnAwacsWithNoSetCallsign_WhenAPlayerCallsAValidAwacs_ThenReturnsResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit()
            };

            var controller = new Awacs.AwacsResponder(playerRepository, new MockUnitRepository(), new MockAirfieldRepository(), false, null, null, "TEST");
            var player = new ITransmission.Player("Dolt", 1, 2);
            var atc = new ITransmission.Awacs("magic");
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, atc);

            const string expectedResponse = "dolt 1 2, magic, 5 by 5";
            var reply = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, reply.ToSpeech());
        }

        [TestMethod]
        public async Task GivenAnAwacsWithNoSetCallsign_WhenAPlayerCallsAnyface_ThenReturnsResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit()
            };

            var controller = new Awacs.AwacsResponder(playerRepository, new MockUnitRepository(), new MockAirfieldRepository(), false, null, null, "TEST");
            var player = new ITransmission.Player("Dolt", 1, 2);
            var atc = new ITransmission.Awacs("anyface");
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, atc);

            const string expectedResponse = "dolt 1 2, overlord, 5 by 5";
            var reply = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, reply.ToSpeech());
        }

        [TestMethod]
        public async Task GivenAnAwacsWithSetCallsign_WhenAPlayerCallsCorrectAwacs_ThenReturnsResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit()
            };

            var controller = new Awacs.AwacsResponder(playerRepository, new MockUnitRepository(), new MockAirfieldRepository(), false, null, "magic", "TEST");
            var player = new ITransmission.Player("Dolt", 1, 2);
            var atc = new ITransmission.Awacs("magic");
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, atc);

            const string expectedResponse = "dolt 1 2, magic, 5 by 5";
            var reply = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, reply.ToSpeech());
        }

        [TestMethod]
        public async Task GivenAnAwacsWithSetCallsign_WhenAPlayerCallsWrongAwacs_ThenReturnsNoResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit()
            };

            var controller = new Awacs.AwacsResponder(playerRepository, new MockUnitRepository(), new MockAirfieldRepository(), false, null, "magic", "TEST");
            var player = new ITransmission.Player("Dolt", 1, 2);
            var atc = new ITransmission.Awacs("overlord");
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, atc);

            var reply = await controller.ProcessTransmission(transmission);

            Assert.IsNull(reply.ToSpeech());
        }

        [TestMethod]
        public async Task GivenAnAwacsWithSetCallsign_WhenAPlayerCallsAnyface_ThenReturnsResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit()
            };

            var controller = new Awacs.AwacsResponder(playerRepository, new MockUnitRepository(), new MockAirfieldRepository(), false, null, "magic", "TEST");
            var player = new ITransmission.Player("Dolt", 1, 2);
            var atc = new ITransmission.Awacs("anyface");
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, atc);

            const string expectedResponse = "dolt 1 2, magic, 5 by 5";
            var reply = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, reply.ToSpeech());
        }
    }
}
