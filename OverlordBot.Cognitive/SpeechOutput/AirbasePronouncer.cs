﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Diagnostics.CodeAnalysis;

namespace RurouniJones.OverlordBot.Cognitive.SpeechOutput
{
    // ReSharper disable once IdentifierTypo
    public class AirbasePronouncer
    {
        /// <summary>
        ///     Pronounces the airbase as a human GCI / ATC without *all* the words and using correct IPA pronunciation
        /// </summary>
        /// <remarks>
        ///     The output of this method is intended to be used in an SSML string for pronunciation by the Azure Speech Service.
        /// </remarks>
        /// <example>
        ///     "Al Dhafra AFB" -> "Al Dhafra"
        ///     "Henderson Executive -> "Henderson"
        ///     "McCarren Intl Airport" -> "McCarren"
        /// </example>
        /// <param name="airbase">The DCS airbase name.</param>
        /// <returns>An SSML compatible string with the colloquial airbase name</returns>
        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        public static string PronounceAirbase(string airbase)
        {
            // TODO - Try and find the phonetic representation of all airbases on caucasus, including the russian carrier
            return airbase.ToLower() switch
            {
                "krymsk" => "<phoneme alphabet=\"ipa\" ph=\"ˈkrɨm.sk\">Krymsk</phoneme>",
                "kutaisi" => "<phoneme alphabet=\"ipa\" ph=\"kuˈtaɪ si\">Kutaisi</phoneme>",
                "mineralnye vody" =>
                "<phoneme alphabet=\"ipa\" ph=\"mʲɪnʲɪˈralʲnɨjə ˈvodɨ\">Mineralnye Vody</phoneme>",
                "gelendzhik" => "<phoneme alphabet=\"ipa\" ph=\"ɡʲɪlʲɪnd͡ʐˈʐɨk\">Gelendzhik</phoneme>",
                "kobuleti" => "<phoneme alphabet=\"ipa\" ph=\"kʰɔbulɛtʰi\">Kobuleti</phoneme>",
                "anapa-vityazevo" => "anapa",
                "krasnodar-center" => "kras center",
                "krasnodar-pashkovsky" => "kras pash",
                // Remove all the ancillary words that we do not care about when spoken. Include the leading space. This is a 
                // catch-all for reducing the length of airfields that do not have a specific pronunciation defined above.
                _ => airbase.Replace(" Airport", "")
                    .Replace(" AB", "")
                    .Replace(" AFB", "")
                    .Replace(" Intl", "")
                    .Replace(" Airfield", "")
                    .Replace(" AFB", "")
                    .Replace(" International", "")
                    .Replace(" Executive", "") // Henderson in Persian gulf
                    .Replace(" Airstrip", "")
                    .Replace(" Island", "")
            };
        }
    }
}
