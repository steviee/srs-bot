﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Microsoft.Extensions.DependencyInjection;
using RurouniJones.OverlordBot.Datastore;
using System;
using System.Collections.Generic;

namespace RurouniJones.OverlordBot.Core
{
    public class RadioControllerFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public RadioControllerFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        // https://stackoverflow.com/questions/53884417/net-core-di-ways-of-passing-parameters-to-constructor
        public RadioController CreateRadioController(
            string serverShortName,
            IAirfieldRepository airfieldRepository,
            IPlayerRepository playerRepository,
            IUnitRepository unitRepository,
            Configuration.ControllerConfiguration controllerConfiguration,
            Configuration.SimpleRadioConfiguration simpleRadioConfiguration,
            ulong discordLogServerId,
            HashSet<AirfieldStatus> airfields)
        {
            return ActivatorUtilities.CreateInstance<RadioController>(_serviceProvider,
                serverShortName,
                airfieldRepository,
                playerRepository,
                unitRepository,
                controllerConfiguration,
                simpleRadioConfiguration,
                discordLogServerId,
                airfields);
        }
    }
}
